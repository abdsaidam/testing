package com.example.aes_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

public class outpording extends AppCompatActivity   {
    ViewPager viewPager ;
    TabLayout tabLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        changeStatusBarColor(R.color.accent);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outpording);
        viewPager =findViewById(R.id.view_pager);
        tabLayout=findViewById(R.id.tap);
        Toolbar toolbar = findViewById(R.id.my_toolbar);
       // toolbar.setTitle("Add Category");
        setSupportActionBar(toolbar);
        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }



    private void setUpViewPager(ViewPager viewPager) {
        SliderPagerAdapter adapter =new SliderPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BlankFragment1(),"");
        adapter.addFragment(new BlankFragment2(),"");
        adapter.addFragment(new BlankFragment(),"");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();



    }
    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(color, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(color));
        }

    }
}
