package com.example.aes_project.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.aes_project.R;
import com.example.aes_project.SliderPagerAdapter;
import com.example.aes_project.Women_Clothing_fragment.Jackets;
import com.example.aes_project.Women_Clothing_fragment.Large_sizes;
import com.example.aes_project.Women_Clothing_fragment.Pants;
import com.example.aes_project.Women_Clothing_fragment.top_sizesand_dresses;
import com.example.aes_project.fragement_deawer.frag_details;
import com.example.aes_project.fragement_deawer.product;
import com.example.aes_project.fragement_deawer.ruterner;
import com.google.android.material.tabs.TabLayout;

public class details_activity extends AppCompatActivity {
    ViewPager viewPage;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_activity);
        findview();
        setUpViewPager(viewPage);
        tabLayout.setupWithViewPager(viewPage);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    public void findview (){
        tabLayout=findViewById(R.id.tap_details);
        viewPage=findViewById(R.id.page);
    }


    private void setUpViewPager(ViewPager viewPager) {
        SliderPagerAdapter adapter = new SliderPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new product(), "المنتج");
      adapter.addFragment(new frag_details(), "التفاصيل");
        adapter.addFragment(new ruterner(), "المرجعات");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
