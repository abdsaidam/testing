package com.example.aes_project.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.aes_project.Adapter.Wedding_dresses;
import com.example.aes_project.Adapter.quick_offer_adapter;
import com.example.aes_project.BaseActivity;
import com.example.aes_project.fragment_pager_widding_adress.frag1;
import com.example.aes_project.fragment_pager_widding_adress.frag2;
import com.example.aes_project.fragment_pager_widding_adress.frag3;
import com.example.aes_project.R;
import com.example.aes_project.SliderPagerAdapter;
import com.example.aes_project.models.quick_offer_model;
import com.example.aes_project.models.weding_dress_model;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class wedding_dresses extends BaseActivity {

    private ViewPager viewPager;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private TabLayout tabLayout1;
    RecyclerView.LayoutManager lm;
    private List<weding_dress_model> itemdata3 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        changeStatusBarColor(R.color.accent);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wedding_dresses);
        findview();
        toolbar();
        setUpViewPager(viewPager);
        tabLayout1.setupWithViewPager(viewPager);
        tabLayout1.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setdata3();
        initializeRecyclerAdapter3();

    }
    public void findview(){
        viewPager = findViewById(R.id.pager_wedding_adress);
        tabLayout1 = findViewById(R.id.tab_wedding_adress);
        recyclerView=findViewById(R.id.wedding_dress_recycle);
        toolbar = findViewById(R.id.my_toolbar);
    }
    private void setUpViewPager(ViewPager viewPager) {
        SliderPagerAdapter adapter = new SliderPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new frag1(), "");
        adapter.addFragment(new frag2(), "");
        adapter.addFragment(new frag3(), "");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    public void setdata3(){
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata3.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));



    }
    public void toolbar (){

      toolbar.setTitle("فساتين زفاف");
        setSupportActionBar(toolbar);
    }
    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        Wedding_dresses adapter= new Wedding_dresses(itemdata3);
        lm = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

}
