package com.example.aes_project.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.aes_project.Adapter.quick_offer_adapter;
import com.example.aes_project.R;
import com.example.aes_project.adapter3;
import com.example.aes_project.model3;
import com.example.aes_project.models.quick_offer_model;

import java.util.ArrayList;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;

public class Quick_offers extends AppCompatActivity {
    CountdownView countDownTimer;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager lm;
    private List<quick_offer_model> itemdata3 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_offers);
        findview();
        setdata3();
        initializeRecyclerAdapter3();
    }
    public void findview(){
        recyclerView=findViewById(R.id.Quickoffers);
        countDownTimer = findViewById(R.id.counter);
        countDownTimer.start(555555555);
    }
    public void setdata3(){
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));
        itemdata3.add(new quick_offer_model(R.drawable.ic_mosza,"جاكيت رجالي","SR 3455","998","70%"));


    }
    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        quick_offer_adapter  adapter= new quick_offer_adapter(itemdata3);
        lm = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }


}
