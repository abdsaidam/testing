package com.example.aes_project.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.aes_project.R;
import com.example.aes_project.adapter3;
import com.example.aes_project.model3;

import java.util.ArrayList;
import java.util.List;

public class Best_selling_products extends AppCompatActivity {
    private RecyclerView.LayoutManager lm;
    private RecyclerView recyclerView3;
    adapter3 Adapter3;
    private List<model3> itemdata3 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_selling_products);
        findview();
        setdata3();
        initializeRecyclerAdapter3();
    }

    public void findview() {
        recyclerView3 = findViewById(R.id.resy2);
    }
    private void initializeRecyclerAdapter3() {

        recyclerView3.setNestedScrollingEnabled(false);
        Adapter3 = new adapter3(itemdata3);
        lm = new GridLayoutManager(this, 2);
        recyclerView3.setLayoutManager(lm);
        recyclerView3.setItemAnimator(new DefaultItemAnimator());
        recyclerView3.setAdapter(Adapter3);
    }
    public void setdata3(){
        itemdata3.add(new model3(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new model3(R.drawable.ic_coos,"شباشب"));
        itemdata3.add(new model3(R.drawable.ic_mekup,"المكياج"));
        itemdata3.add(new model3(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new model3(R.drawable.ic_coos,"شباشب"));
        itemdata3.add(new model3(R.drawable.ic_mekup,"المكياج"));
        itemdata3.add(new model3(R.drawable.ic_coos,"شباشب"));
        itemdata3.add(new model3(R.drawable.ic_mekup,"المكياج"));
        itemdata3.add(new model3(R.drawable.ic_coos,"شباشب"));
        itemdata3.add(new model3(R.drawable.ic_mekup,"المكياج"));
        itemdata3.add(new model3(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new model3(R.drawable.ic_bejama,"بجامات"));

    }
    }
