package com.example.aes_project.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.aes_project.BlankFragment;
import com.example.aes_project.BlankFragment1;
import com.example.aes_project.BlankFragment2;
import com.example.aes_project.R;
import com.example.aes_project.SliderPagerAdapter;
import com.example.aes_project.Women_Clothing_fragment.Jackets;
import com.example.aes_project.Women_Clothing_fragment.Large_sizes;
import com.example.aes_project.Women_Clothing_fragment.Pants;
import com.example.aes_project.Women_Clothing_fragment.top_sizesand_dresses;
import com.google.android.material.tabs.TabLayout;

public class women_clothing extends AppCompatActivity {
    ViewPager viewPage;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_women_clothing);
        findview();
        setUpViewPager(viewPage);
        tabLayout.setupWithViewPager(viewPage);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    public void findview (){
        tabLayout=findViewById(R.id.Women_clothes);
        viewPage=findViewById(R.id.pager_woman_clothes);
    }
    private void setUpViewPager(ViewPager viewPager) {
        SliderPagerAdapter adapter =new SliderPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new top_sizesand_dresses(),"ملابس علوية وفساتين");
        adapter.addFragment(new Jackets(),"جاكيتات");
        adapter.addFragment(new Large_sizes(),"مقاسات كبيرة");
        adapter.addFragment(new Pants(),"بناطيل");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();



    }
}
