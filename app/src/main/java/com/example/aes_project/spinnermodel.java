package com.example.aes_project;

public class spinnermodel {
   private int spinnerphoto;
    private String spinnertext;

    public int getSpinnerphoto() {
        return spinnerphoto;
    }

    public void setSpinnerphoto(int spinnerphoto) {
        this.spinnerphoto = spinnerphoto;
    }

    public String getSpinnertext() {
        return spinnertext;
    }

    public void setSpinnertext(String spinnertext) {
        this.spinnertext = spinnertext;
    }

    public spinnermodel(int spinnerphoto, String spinnertext) {
        this.spinnerphoto = spinnerphoto;
        this.spinnertext = spinnertext;
    }


}
