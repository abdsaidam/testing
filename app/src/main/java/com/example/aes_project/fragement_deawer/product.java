package com.example.aes_project.fragement_deawer;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aes_project.Adapter.Wedding_dresses;
import com.example.aes_project.Adapter.coulour_recycle_adapter;
import com.example.aes_project.R;
import com.example.aes_project.adapter2;
import com.example.aes_project.models.colour_model;
import com.example.aes_project.models.weding_dress_model;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class product extends Fragment {
    RecyclerView.LayoutManager lm;
    private List<weding_dress_model> itemdata4= new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView recyclerView1;
    private View view;
    private List<colour_model> itemdata3 = new ArrayList<>();


    public product() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view= inflater.inflate(R.layout.fragment_product, container, false);
       findview();
       return view;
    }
    public void findview(){

        recyclerView= view.findViewById(R.id.coulor_recycle);
        recyclerView1=view.findViewById(R.id.wedding_dress_recycle);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setdata3();
        initializeRecyclerAdapter2();
        setdata4();
        initializeRecyclerAdapter3();
    }

    public void setdata3(){
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));
        itemdata3.add(new colour_model(R.drawable.selector_coulor));





    }
    private void initializeRecyclerAdapter2() {
        recyclerView.setNestedScrollingEnabled(false);
        coulour_recycle_adapter  adapter = new coulour_recycle_adapter(itemdata3);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    public void setdata4(){
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));
        itemdata4.add(new weding_dress_model(R.drawable.ic_mosza,"dddd","ggggg","hhhhhh"));



    }
    private void initializeRecyclerAdapter3() {

        recyclerView1.setNestedScrollingEnabled(false);
        Wedding_dresses adapter= new Wedding_dresses(itemdata4);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(adapter);
    }

}
