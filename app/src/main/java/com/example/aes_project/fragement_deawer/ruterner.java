package com.example.aes_project.fragement_deawer;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.aes_project.Adapter.coulour_recycle_adapter;
import com.example.aes_project.Adapter.recycle_comment_adapter;
import com.example.aes_project.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ruterner extends Fragment {
    ProgressBar progressBar;
    View view;
    private RecyclerView recyclerView;


    public ruterner() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view = inflater.inflate(R.layout.fragment_ruterner, container, false);
       findview();
       return view;
    }
    public void findview (){
        recyclerView=view.findViewById(R.id.recycle_comment);
        progressBar= view.findViewById(R.id.progressBar);
        progressBar.setProgress(70);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeRecyclerAdapter2();
    }
    private void initializeRecyclerAdapter2() {
        recyclerView.setNestedScrollingEnabled(false);
        recycle_comment_adapter adapter = new recycle_comment_adapter();
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}
