package com.example.aes_project.fragement_deawer;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aes_project.R;
import com.example.aes_project.Adapter.recenr_arrive_adapter;
import com.example.aes_project.models.recently_arrived_model;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class catigory_Fragment extends Fragment {
    private List<recently_arrived_model> modeellist = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;

    View view ;


    public catigory_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



                view =inflater.inflate(R.layout.fragment_catigory_, container, false);
        recyclerView =view.findViewById(R.id.recentlyarrived);
        recyclerView1 =view.findViewById(R.id.Editorspicks);
        recyclerView2 =view.findViewById(R.id.Brand);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setdata();
        initializeRecyclerAdapter();
        initializeRecyclerAdapter1();
        initializeRecyclerAdapter2();
    }

    public void setdata(){
        modeellist.add(new recently_arrived_model(R.drawable.ic_bejama,"بجامات"));
        modeellist.add(new recently_arrived_model(R.drawable.ic_coos,"شباشب"));
        modeellist.add(new recently_arrived_model(R.drawable.ic_mekup,"المكياج"));

    }
    private void initializeRecyclerAdapter() {
        recyclerView.setNestedScrollingEnabled(false);
        recenr_arrive_adapter arrive_adapter =new  recenr_arrive_adapter (modeellist);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(arrive_adapter);
    }
    private void initializeRecyclerAdapter1() {
        recyclerView.setNestedScrollingEnabled(false);
        recenr_arrive_adapter arrive_adapter =new  recenr_arrive_adapter (modeellist);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(arrive_adapter);
    }
    private void initializeRecyclerAdapter2() {
        recyclerView2.setNestedScrollingEnabled(false);
        recenr_arrive_adapter arrive_adapter =new  recenr_arrive_adapter (modeellist);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView2.setLayoutManager(mLayoutManager);
        recyclerView2.setItemAnimator(new DefaultItemAnimator());
        recyclerView2.setAdapter(arrive_adapter);
    }


}
