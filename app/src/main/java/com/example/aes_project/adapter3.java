package com.example.aes_project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class adapter3 extends RecyclerView.Adapter<adapter3.holder3>{
    private List<model3> itemdata = new ArrayList<>();
    View view;

    public adapter3(List<model3> itemdata) {
        this.itemdata = itemdata;
    }

    @NonNull
    @Override
    public holder3 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item4, parent, false);
        return   new holder3(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder3 holder, int position) {
        holder.string1.setText(itemdata.get(position).getString1());

        holder.imageView.setImageResource(itemdata.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return itemdata.size();
    }

    public  class holder3  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string1;




        public holder3(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.bejama);
            string1 =itemView.findViewById(R.id.txtbejama);

        }





    }
}
