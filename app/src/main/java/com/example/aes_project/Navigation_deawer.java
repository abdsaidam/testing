package com.example.aes_project;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.aes_project.fragement_deawer.catigory_Fragment;
import com.google.android.material.navigation.NavigationView;

public class Navigation_deawer extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);changeStatusBarColor(R.color.accent);
        setContentView(R.layout.activity_navigation_deawer);
        Toolbar toolbar = findViewById(R.id.toolbarss);
        navigationView = findViewById(R.id.navview1);
        navigationView.setNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.start, R.string.stop);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.ic_aaa);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.animator.anim);
                drawerView.startAnimation(animFadeIn);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });








    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "vvvvvvvvvvv", Toast.LENGTH_SHORT).show();
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.home:
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new navdrow_1()).commit();
                Toast.makeText(this, "frrrrrrrrrrrrrrrrrrrrrrrrrrrr", Toast.LENGTH_SHORT).show();

                break;
            case R.id.catigory:
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new catigory_Fragment()).commit();

                break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true ;



    }

}

