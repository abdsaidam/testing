package com.example.aes_project;

class Itemdata {
    private int imge ;
    private String stringmarket ;

    public Itemdata(int imge, String stringmarket) {
        this.imge = imge;
        this.stringmarket = stringmarket;
    }

    public int getImge() {
        return imge;
    }

    public void setImge(int imge) {
        this.imge = imge;
    }

    public String getStringmarket() {
        return stringmarket;
    }

    public void setStringmarket(String stringmarket) {
        this.stringmarket = stringmarket;
    }
}
