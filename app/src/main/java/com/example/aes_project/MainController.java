package com.example.aes_project;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Momen Sisalem on 6/19/2016.
 */

public class MainController {
    //public static MainController INSTANCE;

    private static boolean developmentStage = true;

//    /*public static MainController initialize(Context context) {
//        if (INSTANCE == null) {
//            INSTANCE = new MainController(context);
//        }
//
//        return INSTANCE;
//    }
//
//    public static MainController instance() {
//        if (INSTANCE == null) {
//            throw new RuntimeException("Main Controller is not initialized");
//        }
//
//        return INSTANCE;
//    }*/

    public static void printLog(String logTag, String logText) {
        if (developmentStage) {
            Log.d(logTag, logText);
        }
    }

    public static boolean isEmpty(String text) {
        if (text == null) {
            return true;
        }
        if (text.equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    public static Drawable changeDrawableColor(int drawable, int filterColor, PorterDuff.Mode mode) {
        Drawable mDrawable = null;
        mDrawable = compatibilityGetDrawable(drawable);
        mDrawable.mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(compatibilityGetColor(filterColor), mode));
        return mDrawable;
    }

    public static Drawable compatibilityGetDrawable(int drawable) {
        Drawable mDrawable = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDrawable = ContextCompat.getDrawable(getContext(), drawable);
        } else {
            mDrawable = getContext().getResources().getDrawable(drawable);
        }
        return mDrawable;
    }

    public static int compatibilityGetColor(int targetColor) {
        int color = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            color = ContextCompat.getColor(getContext(), targetColor);
        } else {
            color = getContext().getResources().getColor(targetColor);
        }
        return color;
    }

    public static boolean isAppIsInBackground() {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(getContext().getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(getContext().getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public static boolean validateEmail(String email) {
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        }
        return false;
    }

    public static void sendSMS(Context context, String number) {
        try {
            if (number != null && number.equalsIgnoreCase("") && context != null) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setData(Uri.parse("smsto:"));
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", number);
                smsIntent.putExtra("sms_body", "Sms Message here... ");
                context.startActivity(smsIntent);
//            finish();
            }
        } catch (Exception e) {
            Toast.makeText(context, "Failed, try again!", Toast.LENGTH_SHORT).show();
        }
    }

//    public static ProgressDialogController showProgressDialog(Context context, String title, String message) {
//        ProgressDialogController progressDialogController = ProgressDialogBuilder.getInstance()
//                .initialize(context)
//                .setStyle(ProgressDialog.STYLE_SPINNER)
//                .setTitle(title)
//                .setMessage(message)
//                .isCancelable(false)
//                .build();
//        progressDialogController.showProgressDialog();
//        return progressDialogController;
//    }

    public static String getAppVersion() {
        PackageManager manager = getContext().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info.versionName;
    }

    public static List<Integer> getDeviceDimens(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        List<Integer> dimens = new ArrayList<>();
        dimens.add(0, width);
        dimens.add(1, height);
        return dimens;
    }

    public static String getCurrentDateTime(int type) {
        DateFormat dateFormatter = null;
        if (type == 1) {
            dateFormatter = new SimpleDateFormat("yyyy/MM/dd hh:mm");
        } else if (type == 2) {
            dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
        } else if (type == 3) {
            dateFormatter = new SimpleDateFormat("hh:mm a");
        } else if (type == 4) {
            dateFormatter = new SimpleDateFormat("kk:mm a");
        }

        dateFormatter.setLenient(false);
        Date today = new Date();
        return dateFormatter.format(today);
    }

    public static String convertToTimeZone(String currentDate) {
        try {
            SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfIn.setTimeZone(Calendar.getInstance().getTimeZone());
            sdfIn.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdfIn.parse(currentDate);

            SimpleDateFormat sdfOut = new SimpleDateFormat("kk:mm a");
            sdfOut.setTimeZone(TimeZone.getDefault());
            Log.d("TIME_ZONE", "onCreate: " + sdfOut.format(date));
            return sdfOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int getNavigationBarHeight() {
        Resources resources = getContext().getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getContext().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int pxToDp(int px) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private static Context getContext() {
        return AppController.getInstance();
    }

    public void openFacebookPage() {
        String facebookUrl = "https://www.facebook.com/Unit.One.IT";
        try {
            int versionCode = getContext().getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                // open the Facebook app using the old method (fb://profile/id or fb://page/id)
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/27846482815")));
            }
        } catch (PackageManager.NameNotFoundException e) {
            // Facebook is not installed. Open the browser
            getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
    }

    public void openTwitterPage() {
        Intent intent = null;
        try {
            // get the Twitter app if possible
            getContext().getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=1899707299"));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/UnitOne_ICT"));
        }
        getContext().startActivity(intent);
    }

    public void openGooglePlusPage() {
        try {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/111527495716336766410"));
            intent.setPackage("com.google.android.apps.plus");
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                getContext().startActivity(intent);
            }
        } catch (Exception e) {
            getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/111527495716336766410")));
        }
    }

    public void makeCall(String number) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + number));
        getContext().startActivity(callIntent);
    }

    public void sendEmail(View view) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
//        i.putExtra(Intent.EXTRA_EMAIL, new String[]{getContext().getResources().getString(R.string.email_address)});
//                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//                i.putExtra(Intent.EXTRA_TEXT, "body of email");
        try {
            getContext().startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
//            Snackbar.make(view, "No Installed Emails !", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
        }
    }

    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String setPic(String imagePath) {
        // Get the dimensions of the View
        try {
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imagePath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            Log.d("Photo", "setPic: " + photoW + " :: " + photoH);
            int scaleFactor = Math.min(photoW / 600, photoH / 600);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);

            Matrix mtx = new Matrix();
            // Rotating Bitmap
            Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            rotatedBMP.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] byteArrayImage = baos.toByteArray();
            return "data:image/jpeg;base64," + Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static Map<String, String> getCurrentDateAndTime() {
        Map<String, String> map = new HashMap<>();
        Calendar calendar = new GregorianCalendar();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/d", Locale.US);
        String date = dateFormat.format(calendar.getTime());
        map.put("date", date);

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm", Locale.US);
        String time = timeFormat.format(calendar.getTime());
        map.put("time", time);
        return map;
    }

    public static void showWithAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.0f);
        view.animate()
                .setDuration(500)
                .translationY(0)
                .alpha(1.0f)
                .setListener(null);
    }

    public static void showWithAnimation(View view, float yTranslation) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.0f);
        view.animate()
                .setDuration(500)
                .translationY(yTranslation)
                .alpha(1.0f)
                .setListener(null);
    }

    public static void hideWithAnimation(final View view, float yTranslation) {
        view.animate()
                .translationY(yTranslation)
                .setDuration(500)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
    }
}
