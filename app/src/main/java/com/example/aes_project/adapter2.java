package com.example.aes_project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class adapter2  extends RecyclerView.Adapter<adapter2.holder2> {
    View view;

    public adapter2(List<model2> itemdata) {
        this.itemdata = itemdata;
    }

    private List<model2> itemdata = new ArrayList<>();


    @NonNull
    @Override
    public holder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item3, parent, false);
        return   new holder2(view);

    }

    @Override
    public void onBindViewHolder(@NonNull holder2 holder, int position) {
        holder.string1.setText(itemdata.get(position).getString1());
        holder.string2.setText(itemdata.get(position).getString2());
        holder.string3.setText(itemdata.get(position).getString3());

        holder.imageView.setImageResource(itemdata.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return itemdata.size();
    }

    public  class holder2  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string1;
        private TextView string2;
        private TextView string3;



        public holder2(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.moza);
            string1 =itemView.findViewById(R.id.price);
            string2 =itemView.findViewById(R.id.txt7);
            string3 =itemView.findViewById(R.id.txt8);
        }





    }
}
