package com.example.aes_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.WindowInsets;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aes_project.Adapter.adapter_basket;

public class basket_activity extends AppCompatActivity {
    private RecyclerView recyclerView;
    CheckBox cheak;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_activity);
        recyclerView=findViewById(R.id.recycle_basket);
        txt=findViewById(R.id.specify);
        initializeRecyclerAdapter3();


    }
    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        adapter_basket  adapter = new adapter_basket(txt);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}
