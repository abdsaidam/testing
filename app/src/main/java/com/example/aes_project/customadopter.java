package com.example.aes_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class customadopter extends ArrayAdapter<spinnermodel> {
    public customadopter(@NonNull Context context, ArrayList<spinnermodel>customarrau) {
        super(context, 0, customarrau);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return customview(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return customview (position, convertView, parent);
    }
    public View customview ( int position, @Nullable View convertView, @NonNull ViewGroup parent){

        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner2,parent,false);
        }
        spinnermodel model =getItem(position);
        ImageView spinimage=convertView.findViewById(R.id.imagespinner);
        TextView txtspin=convertView.findViewById(R.id.textspinner);
        if (model!=null){
            spinimage.setImageResource(model.getSpinnerphoto());
            txtspin.setText(model.getSpinnertext());
        }
        return  convertView;


    }
}
