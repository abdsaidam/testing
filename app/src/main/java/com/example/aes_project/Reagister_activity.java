package com.example.aes_project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class Reagister_activity extends AppCompatActivity {
    private List<String> categoryNames = new ArrayList<>();
    private Spinner categoriesSpinner;
    private Spinner spinner1;
    ArrayList<spinnermodel>customitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reagister_activity);
        categoriesSpinner=findViewById(R.id.spinngender);
        spinner1=findViewById(R.id.spinngender1);
        setCategoryNames();
        initializeAddressesSpinner();
        customitem();

    }

    private void initializeAddressesSpinner() {
        categoriesSpinner.setSelection(0);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.textview3, categoryNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoriesSpinner.setAdapter(dataAdapter);
    }
    private void setCategoryNames() {

        categoryNames.add("Gender");
        categoryNames.add("Male");
        categoryNames.add("Female");

    }
    public  void customitem (){
        customitem =new ArrayList<>();
        customitem.add(new spinnermodel(R.drawable.ic_intrnet,"Selectcountry"));
     customadopter Customadopter =new customadopter(this,customitem);
     if (spinner1!=null){
         spinner1.setAdapter(Customadopter);
     }
    }

}