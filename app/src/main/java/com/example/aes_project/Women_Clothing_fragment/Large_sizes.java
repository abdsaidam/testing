package com.example.aes_project.Women_Clothing_fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aes_project.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Large_sizes extends Fragment {


    public Large_sizes() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_large_sizes, container, false);
    }

}
