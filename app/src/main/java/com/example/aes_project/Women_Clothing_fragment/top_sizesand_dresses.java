package com.example.aes_project.Women_Clothing_fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aes_project.Adapter.Top_clothes_and_pants;
import com.example.aes_project.R;
import com.example.aes_project.adapter3;
import com.example.aes_project.model3;
import com.example.aes_project.models.Top_clothes_and_pants_model;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class top_sizesand_dresses extends Fragment {
    private RecyclerView recyclerView;
    private View view;
    private List<Top_clothes_and_pants_model> itemdata3 = new ArrayList<>();
   private RecyclerView.LayoutManager lm;


    public top_sizesand_dresses() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
     view=inflater.inflate(R.layout.fragment_top_sizesand_dresses, container, false);
     findview();
     return view;
    }
    public void findview(){
        recyclerView=view.findViewById(R.id.Top_clothes_and_pants_recycle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setdata3();
        initializeRecyclerAdapter3();

    }

    public void setdata3(){
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new Top_clothes_and_pants_model(R.drawable.ic_bejama,"بجامات"));

    }
    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        Top_clothes_and_pants top_clothes_and_pants= new Top_clothes_and_pants(itemdata3);
        lm = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(top_clothes_and_pants);
    }

}
