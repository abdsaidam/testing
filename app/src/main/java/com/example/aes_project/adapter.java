package com.example.aes_project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class adapter   extends RecyclerView.Adapter<adapter.holder>  {
    private List<Itemdata> itemdata = new ArrayList<>();

    public adapter(List<Itemdata> itemdata) {
        this.itemdata = itemdata;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_market, parent, false);
        return  new  holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        holder.string.setText(itemdata.get(position).getStringmarket());
        holder.imageView.setImageResource(itemdata.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return itemdata.size();
    }

    public  class holder  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string;


        public holder(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.imgemarket);
            string =itemView.findViewById(R.id.ttttt);
        }





    }
}
