package com.example.aes_project.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aes_project.R;
import com.example.aes_project.models.recently_arrived_model;

import java.util.ArrayList;
import java.util.List;

public class recenr_arrive_adapter  extends RecyclerView.Adapter<recenr_arrive_adapter.holder3>{
    private List<recently_arrived_model> modeellist = new ArrayList<>();
    View view;

    public recenr_arrive_adapter(List<recently_arrived_model> modeellist) {
        this.modeellist = modeellist;
    }

    @NonNull
    @Override
    public holder3 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recentlyarrived_item, parent, false);
        return   new recenr_arrive_adapter.holder3(view);

    }

    @Override
    public void onBindViewHolder(@NonNull holder3 holder, int position) {
        holder.string1.setText(modeellist.get(position).getString1());

        holder.imageView.setImageResource(modeellist.get(position).getImge());


    }

    @Override
    public int getItemCount() {
        return modeellist.size();
    }

    public  class holder3  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string1;




        public holder3(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.img1);
            string1 =itemView.findViewById(R.id.txt1);

        }





    }
}
