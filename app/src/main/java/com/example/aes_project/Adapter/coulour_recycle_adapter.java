package com.example.aes_project.Adapter;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aes_project.R;
import com.example.aes_project.models.colour_model;
import com.example.aes_project.models.recently_arrived_model;

import java.util.ArrayList;
import java.util.List;

public class coulour_recycle_adapter extends RecyclerView.Adapter<coulour_recycle_adapter.holder2> {
    private List<colour_model> modeellist = new ArrayList<>();
    View view;

    public coulour_recycle_adapter(List<colour_model> modeellist) {
        this.modeellist = modeellist;
    }

    @NonNull
    @Override
    public holder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.colour_item_recycle, parent, false);
        return   new coulour_recycle_adapter.holder2(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder2 holder, int position) {

       holder. textView.setBackgroundResource(modeellist.get(position).getCoulour());

    }

    @Override
    public int getItemCount() {
        return modeellist.size();
    }

    public  class holder2  extends RecyclerView.ViewHolder  {
        private TextView textView;





        public holder2(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){

            textView =itemView.findViewById(R.id.coulour);



        }






    }
}
