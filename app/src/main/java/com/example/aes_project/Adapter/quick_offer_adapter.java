package com.example.aes_project.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aes_project.R;
import com.example.aes_project.adapter2;
import com.example.aes_project.model2;
import com.example.aes_project.models.quick_offer_model;

import java.util.ArrayList;
import java.util.List;

public class quick_offer_adapter  extends RecyclerView.Adapter<quick_offer_adapter.holder2> {
    View view;
    private List<quick_offer_model> itemdata = new ArrayList<>();


    @NonNull
    @Override
    public holder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quick_offers, parent, false);
        return   new quick_offer_adapter.holder2(view);
    }

    public quick_offer_adapter(List<quick_offer_model> itemdata) {
        this.itemdata = itemdata;
    }

    @Override
    public void onBindViewHolder(@NonNull holder2 holder, int position) {
        holder.string1.setText(itemdata.get(position).getString1());
        holder.string2.setText(itemdata.get(position).getString2());
        holder.string3.setText(itemdata.get(position).getString3());
        holder.string4.setText(itemdata.get(position).getString4());

        holder.imageView.setImageResource(itemdata.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return itemdata.size();
    }

    public  class holder2  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string1;
        private TextView string2;
        private TextView string3;
        private TextView string4;



        public holder2(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.offerimg);
            string1 =itemView.findViewById(R.id.priceoffer);
            string2 =itemView.findViewById(R.id.nameoffer);
            string3 =itemView.findViewById(R.id.oldnum);
            string4 =itemView.findViewById(R.id.percntage);
        }





    }
}
