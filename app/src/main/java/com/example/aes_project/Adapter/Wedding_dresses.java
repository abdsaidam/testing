package com.example.aes_project.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aes_project.R;
import com.example.aes_project.models.weding_dress_model;

import java.util.ArrayList;
import java.util.List;

public class Wedding_dresses extends RecyclerView.Adapter<Wedding_dresses.holder2> {
    private List<weding_dress_model> modeellist = new ArrayList<>();
    View view;

    @NonNull
    @Override
    public holder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wedding_dress_recycle_item, parent, false);
        return   new Wedding_dresses.holder2(view);
    }

    public Wedding_dresses(List<weding_dress_model> modeellist) {
        this.modeellist = modeellist;
    }

    @Override
    public void onBindViewHolder(@NonNull holder2 holder, int position) {
        holder.string1.setText(modeellist.get(position).getString1());
        holder.string2.setText(modeellist.get(position).getString2());
        holder.string3.setText(modeellist.get(position).getString3());

        holder.imageView.setImageResource(modeellist.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return modeellist.size();
    }


    public  class holder2  extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView string1;
        private TextView string2;
        private TextView string3;




        public holder2(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.imgweding);
            string1 =itemView.findViewById(R.id.nameproduct);
            string2 =itemView.findViewById(R.id.price1);
            string3 =itemView.findViewById(R.id.discription);

        }





    }
}
