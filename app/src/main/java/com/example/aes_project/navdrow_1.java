package com.example.aes_project;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aes_project.fragement_deawer.catigory_Fragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.iwgang.countdownview.CountdownView;


/**
 * A simple {@link Fragment} subclass.
 */
public class navdrow_1 extends Fragment {
    private List<Itemdata> itemdata = new ArrayList<>();
    private List<model2> itemdata2 = new ArrayList<>();
    private List<model3> itemdata3 = new ArrayList<>();

    int curcer = 0;
    private adapter4 Adapter;
    private ImageView imageView;
    private  static    boolean runner =true;
    private adapter3 Adapter3;
    private ViewPager viewPager;
    private TabLayout tabLayout1;
    adapter2 Adapter2;
    private RecyclerView recyclerView;
    CountdownView countDownTimer;
    LinearLayoutManager lm;
    Timer timer;
    private RecyclerView recyclerView4;
    int itemcount;
    private RecyclerView recyclerView2;
    private RecyclerView recyclerView3;
    private long mstarttimeinmilli;
    private CountDownTimer mcountDownTimer;
    private Threditemrecview threditemrecview;
    private boolean mtimerruning;
    private long mlefttimeinmilli;
    private long endtime;
    private adapter adapters;
    private  TextView textView1;
    private  TextView textView2;
    private  TextView textView3;
    private  TextView textView4;

    private View view;
    private adapter4 Adapter4;
    private Fragment fragment;


    public navdrow_1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_navdrow_1, container, false);
        viewPager = view.findViewById(R.id.pager5);
        tabLayout1 = view.findViewById(R.id.tab_12);
        recyclerView = view.findViewById(R.id.recy);
        recyclerView2 = view.findViewById(R.id.resy1);
        recyclerView3 = view.findViewById(R.id.resy2);
        recyclerView4 = view.findViewById(R.id.resy4);
        imageView = view.findViewById(R.id.imge);
        textView1 =view.findViewById(R.id.txt1);
        textView2 =view.findViewById(R.id.txt2);
        textView3 =view.findViewById(R.id.txt3);
        textView4 =view.findViewById(R.id.txt4);

        countDownTimer = view.findViewById(R.id.counter);
        fragment = this;


        Log.e("tag", "navdrow_1 0");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViewPager(viewPager);
        tabLayout1.setupWithViewPager(viewPager);
        tabLayout1.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setdata();
        setdata2();
        setdata3();
        setdata4();
        initializeRecyclerAdapter();
        initializeRecyclerAdapter2();
        initializeRecyclerAdapter3();
        initializeRecyclerAdapter4();
        //  itemrec4();
        threditemrecview =new Threditemrecview();
        threditemrecview.start();




        countDownTimer.start(555555555);


    }









    private void setUpViewPager(ViewPager viewPager) {
        SliderPagerAdapter adapter = new SliderPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new catigory_Fragment(), "");
        adapter.addFragment(new navdrow3(), "");
        adapter.addFragment(new navdraw4(), "");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    private void initializeRecyclerAdapter() {
        recyclerView.setNestedScrollingEnabled(false);
        adapters = new adapter(itemdata);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapters);
    }
    public void setdata(){
        itemdata.add(new Itemdata(R.drawable.ic_icon,"ملابس نسائية"));
        itemdata.add(new Itemdata(R.drawable.ic_rejali,"رجالي"));
        itemdata.add(new Itemdata(R.drawable.ic_choes,"أحذية"));
        itemdata.add(new Itemdata(R.drawable.ic_hakaeb,"حقائب"));
        itemdata.add(new Itemdata(R.drawable.ic_ikseswar,"اكسسوارات"));
        itemdata.add(new Itemdata(R.drawable.ic_sasas,"ملابس للنوم"));
        itemdata.add(new Itemdata(R.drawable.ic_mmmm,"ملابس داخلية"));
        itemdata.add(new Itemdata(R.drawable.ic_read,"رياضي"));
        itemdata.add(new Itemdata(R.drawable.ic_qqqq,"الأم والطفل"));
        itemdata.add(new Itemdata(R.drawable.ic_kkkkkkk,"ملابس كبيرة"));


    }
    public void setdata2(){
    itemdata2.add(new model2(R.drawable.ic_mosza,"SR 58.2","معطف طويل واقفة","78.72"));
    itemdata2.add(new model2(R.drawable.ic_zarga,"SR 58.2","طقم بجامة نسائي","78.72"));
    itemdata2.add(new model2(R.drawable.ic_jaket,"SR 58.2","معطف رجالي","78.72"));
    }
    private void initializeRecyclerAdapter2() {
        recyclerView.setNestedScrollingEnabled(false);
        Adapter2 = new adapter2(itemdata2);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView2.setLayoutManager(mLayoutManager);
        recyclerView2.setItemAnimator(new DefaultItemAnimator());
        recyclerView2.setAdapter(Adapter2);
    }
    public void setdata3(){
        itemdata3.add(new model3(R.drawable.ic_bejama,"بجامات"));
        itemdata3.add(new model3(R.drawable.ic_coos,"شباشب"));
        itemdata3.add(new model3(R.drawable.ic_mekup,"المكياج"));

    }
    private void initializeRecyclerAdapter3() {
        recyclerView3.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState==RecyclerView.SCROLL_STATE_IDLE){
                    curcer=lm.findLastVisibleItemPosition();
                }
            }
        });
        recyclerView3.setNestedScrollingEnabled(false);
        Adapter3 = new adapter3(itemdata3);
        lm = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView3.setLayoutManager(lm);
        recyclerView3.setItemAnimator(new DefaultItemAnimator());
        recyclerView3.setAdapter(Adapter3);
    }
    public void setdata4(){
        itemdata2.add(new model2(R.drawable.ic_mosza,"SR 58.2","معطف طويل واقفة","78.72"));
        itemdata2.add(new model2(R.drawable.ic_zarga,"SR 58.2","طقم بجامة نسائي","78.72"));
        itemdata2.add(new model2(R.drawable.ic_jaket,"SR 58.2","معطف رجالي","78.72"));
    }
    private void initializeRecyclerAdapter4() {
        recyclerView4.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState==RecyclerView.SCROLL_STATE_IDLE){
                    curcer=lm.findLastVisibleItemPosition();
                    curcer=itemcount;
                    imageView.setImageResource(itemdata2.get(curcer).getImge());
                    curcer++;



                }
            }
        });
        recyclerView4.setNestedScrollingEnabled(false);
        Adapter4 = new adapter4(itemdata2);
        lm = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView4.setLayoutManager(lm);
        recyclerView4.setItemAnimator(new DefaultItemAnimator());
        recyclerView4.setAdapter(Adapter4);
    }
    public void  itemrec4 (){

        int i=itemcount;
        for (  i=0;i<=itemdata2.size();i++){
            int s=0;
            imageView.setImageResource(itemdata2.get(s).getImge());
            s++;


        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (timer!=null){
            timer.cancel();



        }
    }

    @Override
    public void onStart() {
         itemcount =recyclerView4.getAdapter().getItemCount();


        super.onStart();
        timer =new Timer();
        timer.schedule(new timer(itemcount),2,1*2000);



    }



    class Threditemrecview extends Thread {

        int s = 0;

        public void run() {
            while (runner){
                s++;



            try {
                Thread.sleep(1000);

            } catch (Exception ex) {
            }if (getActivity()==null)
                return;


          getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    imageView.setImageResource(itemdata2.get(s).getImge());
                    textView1.setText(itemdata2.get(s).getString1());
                    textView2.setText(itemdata2.get(s).getString2());
                    textView3.setText(itemdata2.get(s).getString3());


                }
            });

      if (s==itemdata2.size()-1) {
          s=0;

      }}




    }
    }



    class timer extends TimerTask {
        final int count ;
        timer(int count) {
            this.count = count;

        }




        @Override
        public void run() {

            if (curcer<count){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lm.scrollToPosition(curcer);
                        curcer++;
                    }
                });

            }
            if (curcer>=count){
                curcer=0;
            }

        }
    }
}


