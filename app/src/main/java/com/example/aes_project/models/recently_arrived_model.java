package com.example.aes_project.models;

public class recently_arrived_model
{
    private int imge ;
    private String string1 ;

    public int getImge() {
        return imge;
    }

    public void setImge(int imge) {
        this.imge = imge;
    }

    public String getString1() {
        return string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public recently_arrived_model(int imge, String string1) {
        this.imge = imge;
        this.string1 = string1;
    }
}
