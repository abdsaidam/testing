package com.example.aes_project;

import android.app.FragmentManager;
import android.os.Build;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class BaseActivity extends AppCompatActivity {

    protected FragmentTransaction transaction;
    protected FragmentManager fm;
    protected String currentFragment = "";






    public void replaceFragment(Fragment fragment, String tag, int container, boolean addToBackStack) {
        currentFragment = tag;

        androidx.fragment.app.FragmentManager manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        if (!addToBackStack) {
            transaction.replace(container, fragment, tag);
        } else {
            transaction.replace(container, fragment, tag);
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }
    public void hideStatusBar() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(color, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(color));
        }
    }


}