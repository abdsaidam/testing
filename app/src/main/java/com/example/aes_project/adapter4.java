package com.example.aes_project;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class adapter4  extends RecyclerView.Adapter<adapter4.holder4> {
    public adapter4(List<model2> itemdata) {
        this.itemdata = itemdata;
    }

    private List<model2> itemdata = new ArrayList<>();
    View view;


    @NonNull
    @Override
    public holder4 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_card1sliderviewpager6fragment, parent, false);
        return   new holder4(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder4 holder, int position) {
        holder.textView1.setText(itemdata.get(position).getString1());
        holder.textView2.setText(itemdata.get(position).getString2());
        holder.textView3.setText(itemdata.get(position).getString3());

        holder.imageView.setImageResource(itemdata.get(position).getImge());

    }

    @Override
    public int getItemCount() {
        return itemdata.size();
    }

    public  class holder4  extends RecyclerView.ViewHolder  {
        CountDownTimer timer;
        private ImageView imageView;
      private  TextView textView1;
      private  TextView textView2;
      private  TextView textView3;
      private  TextView textView4;





        public holder4(@NonNull View itemView) {
            super(itemView);
            findview();

        }
        public void findview (){
            imageView =itemView.findViewById(R.id.imge);
            textView1 =itemView.findViewById(R.id.txt1);
            textView2 =itemView.findViewById(R.id.txt2);
            textView3 =itemView.findViewById(R.id.txt3);
            textView4 =itemView.findViewById(R.id.txt4);

        }





    }
}
